package com.codebind;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.util.Vector;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Math.pow;


public class SampleForm extends JFrame{


    private final String VARIABLE_REGEXP = "x\\d+";
    private JButton button_msg;
    private JPanel panelMain;
    private JComboBox predefinedFunc;
    private JTextField manualFunc;
    private JRadioButton action_min;
    private JRadioButton action_max;
    private JPanel funcProp;
    private JPanel algProp;
    private JSpinner minX_spinner;
    private JSpinner maxX_spinner;
    private JButton start;
    private JButton showGraph;
    private JComboBox algoType;
    private JSpinner popSize;
    private JSpinner iterationCount;
    private JSpinner num_cycles;
    private JTextArea resultArea;
    private JSpinner digitsAfterDotSpinner;
    private String function = "";
    private boolean mode;
    private double minX,maxX;
    private int population,iterations,cycles, precision;
    private final XYSeries funcGraph = new XYSeries("Function");

    private SampleForm() {
        start.addActionListener(e -> {
            funcGraph.clear();
            resultArea.setText(null);
            showGraph.setEnabled(true);
            if (manualFunc.getText().length() > 1) function = manualFunc.getText();
            else function = predefinedFunc.getSelectedItem().toString();

            minX = (double) (Integer) minX_spinner.getValue();
            maxX = (double) (Integer) maxX_spinner.getValue();
            population = (Integer) popSize.getValue();
            iterations = (Integer) iterationCount.getValue();
            cycles = (Integer) num_cycles.getValue();
            precision = (Integer) digitsAfterDotSpinner.getValue();

            mode = action_max.isSelected();

            Pattern pattern = Pattern.compile(VARIABLE_REGEXP);
            Matcher m = pattern.matcher(function);
            ArrayList<String> Variables = new ArrayList<>();
            while(m.find()){
                String s =m.group();
                if(!Variables.contains(s)) Variables.add(m.group());
            }

            Problem prob = new Problem(Variables.size());
            prob.setMin(minX);
            prob.setMax(maxX);
            prob.setFunction(function);
            prob.setVariables(Variables);
            prob.setMode(mode);
            resultArea.append("f: "+function+"\n");
            for(int i=0;i<cycles;i++) {
                NondominatedPopulation result = new Executor()
                        .withProblem(prob)
                        .withAlgorithm(algoType.getSelectedItem().toString())
                        .withMaxEvaluations(iterations)
                        .withProperty("populationSize", population)
                        .distributeOnAllCores()
                        .run();
                for (Solution solution : result) {
                    for(int j=0;j<Variables.size();j++)
                        resultArea.append(setPrecision(((RealVariable) solution.getVariable(j)).getValue(),precision)+"\t");
                    double res;
                    if(mode)  res=setPrecision(Vector.negate(solution.getObjectives())[0],precision);
                    else res=setPrecision(solution.getObjectives()[0],precision);
                    resultArea.append(res+"\n");
                    funcGraph.add(iterations,res);
                }
                iterations*=2;
            }
        });
        showGraph.addActionListener(e -> {
            XYDataset xyDataset = new XYSeriesCollection(funcGraph);
            JFreeChart chart = ChartFactory.createXYLineChart("Function values over iterations", "iteration", "f(x)", xyDataset, PlotOrientation.VERTICAL, true , true, false);
            ChartPanel chPanel = new ChartPanel(chart); //creating the chart panel, which extends JPanel
            chPanel.setPreferredSize(new Dimension(785, 440)); //size according to my window
            chPanel.setMouseWheelEnabled(true);
            NumberAxis yAxis = (NumberAxis) chart.getXYPlot().getRangeAxis();
            yAxis.setAutoRangeIncludesZero(false);
            yAxis.setAutoRange(true);
            JPanel jPanel = new JPanel();
            jPanel.add(chPanel); //add the chart viewer to the JPanel
            final JDialog frame = new JDialog(this, "Graph", true);
            frame.getContentPane().add(chPanel);
            frame.pack();
            frame.setVisible(true);
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Genetic Algorithms based on MOEA framework");
        frame.setContentPane(new SampleForm().panelMain);
        frame.setPreferredSize(new Dimension(600, 700));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private double setPrecision(double d, int precision){
        d = d * pow(10,precision);
        int i = (int) Math.round(d);
        d = (double)i / pow(10,precision);
        return d;
    }

}
