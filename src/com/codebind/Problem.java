package com.codebind;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.problem.AbstractProblem;
import org.moeaframework.util.Vector;

import java.util.ArrayList;


public class Problem extends AbstractProblem {

    private String function = "";
    private int numberOfVariables;
    private int numberOfObjectives;
    private double minX;
    private double maxX;
    private boolean mode;
    private ArrayList<String> Variables;

    public Problem(int noV){
        super(noV,1);
        setNumberOfVariables(noV);
        numberOfObjectives=1;
    }

    @Override
    public Solution newSolution() {
        Solution solution = new Solution(getNumberOfVariables(),
                getNumberOfObjectives());

        for (int i = 0; i < getNumberOfVariables(); i++) {
            solution.setVariable(i, new RealVariable(minX, maxX));
        }

        return solution;
    }

    @Override
    public void evaluate(Solution solution) {
        Double[] x = new Double[numberOfVariables];
        for(int i=0;i<numberOfVariables;i++)
            x[i] = ((RealVariable)solution.getVariable(i)).getValue();
        Expression expression = new ExpressionBuilder(function)
               .variables(Variables.toArray(new String[0]))
               .build();
        for(int i=0;i<Variables.size();i++)
            expression.setVariable("x"+(i+1),x[i]);
        double f = expression.evaluate();
        if(mode) solution.setObjectives(Vector.negate(new double[]{f}));
        else solution.setObjectives(new double[]{f});
    }

    public int getNumberOfConstraints() {
        return numberOfConstraints;
    }

    public int getNumberOfObjectives() {
        return numberOfObjectives;
    }

    public int getNumberOfVariables() {
        return numberOfVariables;
    }

    private void setNumberOfVariables(int variables){
        this.numberOfVariables = variables;
    }

    public void setMin(double min){
        this.minX = min;
    }

    public void setMax(double max){
        this.maxX = max;
    }

    public void setMode(boolean mode){
        this.mode = mode;
    }

    public void setFunction(String functionMain){
       this.function = functionMain;
    }

    public void setVariables(ArrayList<String> variables){
        this.Variables = variables;
    }
}
